# CROCO

## The project

The project is to replace the module ```sl``` already existing. Here we want to have a crocodile passing through instead of a train. It is because this project is proposed by Sinfonie, the association of the students of the CS department of the ENS Rennes.

### Build

You can clone the code via the repository:
```bash
  $ git clone https://gitlab.com/TomSoucies/croco_dc.git
```

This project is quite easy to build, you just need to make it in the source folder:
```bash
  $ make
```

To see a crocodile kidnap your screen just enter the following command:
```bash
  $ ./sl
```

You can also have all sorts of crocodiles by adding some options. To see what you can do, just enter:
```bash
  $ ./sl -h
```
To display the following help:
```bash
This project is to replace sl with a crocodile.

Usage:	sl [OPTION]
The option changes the crocodile that appears.
If no option is given, a random crocodile (or an egg) will be displayed.
To add this to your system, follow the README instructions.
Options:
    --help, -h        Display this help and exit
    --version, -v     Show the version of the code
    big               Display the big crocodile
    egg               Display the egg
    small             Display the small crocodile
```


## Code

The code is written in C and is really simple but let's get into so you can add your contribution.

### Architecture 

The project is constructed around 2 main folders. The first can be easily ignored. It is just a folder of all the crocodiles that we have created in ascii shape. The second one is the source folder with all the files that you need to understand the code.

### Main file

The main file gets the options you put in your command line. It also calls the function that will display the crocodile asked for.
To add a crocodile you need to add the option required to display it and the option to make it appear.

### Utils

It is just some functions you may need to create your crocodile.

### Crocodiles

Each crocodile has its files. There is a header where its dimensions and shapes are defined. There is also a file that defines how it is displayed. You can add your crocodile by creating a new file with the same structure as the others.


## Add the command to your PATH

To see the true potential of this project follow the process:

### Create your binary

Download this code then in the repository run:

```bash
  $ make
```

Now that you have your binary, you can use it to access it where ever and when ever you want. Be careful, if you change something on the code you will have to recompile it and do again the following steps.

### Export the binary

Copy the binary to your bin folder:

```bash
  $ cp /path/to/your/binary/sl /usr/local/bin
```

### Add the binary to your bash

Open your bash file and add the following line:

```bash
  export PATH="$HOME/local/bin:$PATH"
```

### Discover the magic

Now you can use the command line:

```bash
  $ sl
```

And every time you fail to use the command ls you will see an extraordinary crocodile eating an egg on your screen.


## Next updates

This repo can be completed with other crocodiles (like on fire, with a hat, etc.) and other animals (like a cat, a dog, a bird, etc.). If you want to contribute, you can fork this repo and make a pull request.
Stay tuned to see the next updates.