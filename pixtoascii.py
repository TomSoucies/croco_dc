#!/usr/bin/env python3

from PIL import Image
import numpy as np

im = Image.open("egg.png", 'r')
width, height = im.size

MAX_SIZE = 250
max_size = max(width, height)
if max_size > MAX_SIZE:
    im = im.resize(((width*MAX_SIZE)//max_size, (height*MAX_SIZE)//max_size))
    width, height = im.size

pix_list = list(im.getdata())
grey_pix_list = np.array([sum(pix)//3 for pix in pix_list])
#grey_pix_list = np.array([pix_list])

pix_tab = np.reshape(grey_pix_list, (height, width))

# char_list = " .,+*%#"
# char_list = "#@%x*;,. "
char_list = "#@:-+x*;,.o "
char_length = len(char_list)-1


ascii_tab = ["" for l in range(height)]
for l in range(height):
    for c in range(width):
        v = pix_tab[l][c]
        achar = char_list[(v*char_length)//255]
        ascii_tab[l] += achar*2

for i in ascii_tab:
    print(i)

im.close()