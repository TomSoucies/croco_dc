#include <curses.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include "egg.h"
#include "utils.h"
#include "big_croco.h"
#include "small_croco.h"

void help()
{
  printf("This project is to replace sl with a crocodile.\n\n");
  printf("Usage:\tsl [OPTION]\n");
  printf("The option changes the crocodile that appears.\n");
  printf("If no option is given, a random crocodile (or an egg) will be displayed.\n");
  printf("To add this to your system, follow the README instructions.\n");
  printf("Options:\n");
  printf("\t--help, -h\t\tDisplay this help and exit\n");
  printf("\t--version, -v\t\tShow the version of the code\n");
  printf("\tbig\t\t\tDisplay the big crocodile\n");
  printf("\tegg\t\t\tDisplay the egg\n");
  printf("\tsmall\t\t\tDisplay the small crocodile\n");
}

int main(int argc, char *argv[])
{

// Choice of crocodile
  char* c = "random";

// Check for options arguments
  if (argc > 1) {
    if (strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "-h") == 0) {
      help();
      return 0;
    }
    else if (strcmp(argv[1], "--version") == 0 || strcmp(argv[1], "-v") == 0) {
      printf("Version 2.0.2\n");
      return 0;
    }
    else if (strcmp(argv[1], "egg") == 0) {
      c = "egg";
    }
    else if (strcmp(argv[1], "big") == 0) {
      c = "big";
    }
    else if (strcmp(argv[1], "small") == 0) {
      c = "small";
    }
    else {
      printf("Invalid option: %s\n\n", argv[1]);
      help();
      return 1;
    }
  }

// Initialise ncurses
  initscr();
  signal(SIGINT, SIG_IGN);
  noecho();
  curs_set(0);
  nodelay(stdscr, TRUE);
  leaveok(stdscr, TRUE);
  scrollok(stdscr, FALSE);

// Chose the animation
  if (strcmp(c, "big") == 0) {
    display_big_croco();
  }
  if (strcmp(c, "egg") == 0) {
    display_egg();
  }
  if (strcmp(c, "small") == 0) {
    display_small_croco();
  }
  if (strcmp(c, "random") == 0) {
    srand(time(NULL));
    int r = rand() % 100;
    if (r < 30) {
      display_big_croco();
    }
    else if (r < 95) {
      display_small_croco();
    }
    else {
      display_egg();
    }
  }

// Close ncurses
  mvcur(0, COLS - 1, LINES - 1, 0);
  endwin();

  return 0;
}