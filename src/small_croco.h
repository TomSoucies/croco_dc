#include <curses.h>
#include <signal.h>
#include <unistd.h>
#include "utils.h"

void display_small_croco();
int add_small_croco (int x);

#define S_CROCO_PATTERNS 2
#define S_CROCO_HEIGHT 5
#define S_CROCO_LENGTH 20

#define S_CROCO_1_1 "  ____"
#define S_CROCO_1_2 " /"
#define S_CROCO_1_3 "/ _._._._(-)-)___."
#define S_CROCO_1_4 "\\__________,,,,,,;"
#define S_CROCO_1_5 " /      \\"

#define S_CROCO_2_1 "  ____"     
#define S_CROCO_2_2 " /"
#define S_CROCO_2_3 "/ _._._._(-)-)___."
#define S_CROCO_2_4 "\\__________,,,,,,;"
#define S_CROCO_2_5 "  \\    /" 