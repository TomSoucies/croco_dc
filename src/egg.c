#include <curses.h>
#include <signal.h>
#include <unistd.h>
#include "utils.h"
#include "egg.h"

void display_egg() {

  int x;

  for (x = 0; ; ++x) {
    clean();
    add_egg();
    if (x > 200) {
      my_mvaddstr(10, 30, "Happy Easter!");
    }
    if (x > 500) {
      my_mvaddstr(30, 30, "Don't worry, you are half way through");
    }
    if (x > 800) {
      my_mvaddstr(50, 30, "You can do it!");
    }
    refresh();
    usleep(40000);
    if (x > 1000)  break;
  }

}

int add_egg ()
{
  static char* egg[EGGHEIGHT + 1] = {
    EGG01, EGG02, EGG03, EGG04, EGG05, EGG06, EGG07, EGG08, EGG09, EGG10, EGG11, EGG12, EGG13, EGG14, EGG15, EGG16, EGG17, EGG18, EGG19,
  };

  int y, i;

  y = LINES / 2 - 1;

  for (i = 0; i < EGGHEIGHT; ++i) {
    my_mvaddstr(y + i, COLS / 2, egg[i]);
  }

  return OK;

}