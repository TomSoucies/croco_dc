#include <curses.h>
#include <signal.h>
#include <unistd.h>
#include "utils.h"

int my_mvaddstr(int y, int x, char *str)
{
  for ( ; x < 0; ++x, ++str)
    if (*str == '\0')  return ERR;
  for ( ; *str != '\0'; ++str, ++x)
    if (mvaddch(y, x, *str) == ERR)  return ERR;
  return OK;
}

int clean()
{
  int i, j;
  for (i = 0; i < LINES; ++i) {
    for (j = 0; j < COLS; ++j) {
      mvaddch(i, j, ' ');
    }
  }
  return 0;
}