#include <curses.h>
#include <signal.h>
#include <unistd.h>
#include "utils.h"
#include "small_croco.h"

void display_small_croco() {

  int x;

  for (x = - (S_CROCO_LENGTH - 10); ; ++x) {
    clean();
    if (add_small_croco(x) == ERR)  break;
    refresh();
    usleep(40000);
  }

}

int add_small_croco (int x)
{
  static char* croco[S_CROCO_PATTERNS][S_CROCO_HEIGHT] = {
    {S_CROCO_1_1, S_CROCO_1_2, S_CROCO_1_3, S_CROCO_1_4, S_CROCO_1_5},
    {S_CROCO_2_1, S_CROCO_2_2, S_CROCO_2_3, S_CROCO_2_4, S_CROCO_2_5}
  };

  int y, i;

  if (x > COLS) return ERR;

  y = 10;

  for (i = 0; i < S_CROCO_HEIGHT; ++i) {
    my_mvaddstr(y + i, x, croco[(S_CROCO_LENGTH + (x / 2)) % S_CROCO_PATTERNS][i]);
  }

  return OK;

}